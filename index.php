<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>laba_1</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <img src="img/maxresdefault.jpg" alt="logo" width="300px">
        <p>Задание для самостоятельной работы «Hello, World!»</p>
    </header>

    <main>
    <?php
        echo'Hello, World!';
    ?>
    </main>

    <footer>
        <p>Задание для самостоятельной работы</p>
        <p>Кулакова Екатерина 211-323</p>
        <p>09.03.2022</p>
    </footer>
</body>
</html>